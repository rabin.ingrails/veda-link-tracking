import mongoose from 'mongoose';

const DB_URI = process.env.DB_URI;
const DB_NAME = process.env.DB_NAME;

async function initializeDB() {
    try {
        await mongoose.connect(DB_URI, {
            dbName: DB_NAME,
        });
        console.log('Connected successfully to server');
    } catch (err) {
        console.log(err);
        throw err;
    }
}

export { initializeDB };
