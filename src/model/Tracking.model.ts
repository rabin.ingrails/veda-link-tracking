import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const trackingSchema = new Schema(
    {
        origin: String,
        referer: String,
        ipAddress: String,
        device: String,
        browser: String,
        os: String,
        createdAt: { type: Date, default: Date.now },
    },
    {
        toJSON: {
            virtuals: true,
            versionKey: false,
            transform: (_, ret) => {
                ret.id = ret._id; // Replace _id with id
                delete ret._id; // Delete _id field
                delete ret.__v; // Delete __v field
            },
        },
    }
);

export default mongoose.model('Tracking', trackingSchema);
