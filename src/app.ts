import 'dotenv/config';

import express from 'express';
import { initializeDB } from './database';
import {
    findAllTrackingController,
    updateTrackingController,
} from './controller';

const app = express();
const port = 3000;

initializeDB()
    .then(() => {
        app.get('/tracking', (req, res) => {
            updateTrackingController(req, res);
        });

        app.get('/tracking/all', (req, res) => {
            findAllTrackingController(req, res);
        });

        app.get('/tracking/health', (req, res) => {
            res.send('OK');
        });
        app.get('/tracking/headers', (req, res) => {
            const xreal = req.headers['x-real-ip'];
            const xforwarded = req.headers['x-forwarded-for'];
            res.send({ xreal, xforwarded, other: req.headers });
        });

        app.get('*', (req, res) => {
            res.send('Not Found');
        });

        app.listen(port, () => {
            console.log(`Express is listening on http://localhost:${port}`);
        });
    })
    .catch((error) => {
        console.error('Failed to initialize database:', error);
    });
