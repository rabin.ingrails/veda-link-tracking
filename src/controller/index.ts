import { Request, Response } from 'express';
import * as useragent from 'useragent';
import TrackingModel from '../model/Tracking.model';

export const updateTrackingController = async (req: Request, res: Response) => {
    const origin = req.query.origin;
    const referer = req.headers.referer;
    const agent = useragent.parse(req.headers['user-agent']);

    const device = agent.device.family.toString().toLowerCase();
    const browser = agent.family.toString().toLowerCase();
    const os = agent.os.family.toString().toLowerCase();

    const ipAddress =
        req.headers['x-forwarded-for'] || req.socket.remoteAddress;

    //store to database
    const newTracking = new TrackingModel({
        origin: origin || '',
        ipAddress: ipAddress || '',
        device: device || '',
        browser: browser || '',
        os: os || '',
        referer: referer || '',
    });

    await newTracking.save();

    res.writeHead(302, {
        Location: process.env.TARGET + '?source=onlinekhabar',
    });
    res.end();
};

export const findAllTrackingController = async (
    req: Request,
    res: Response
) => {
    const trackingList = await TrackingModel.find();
    const total = await TrackingModel.countDocuments();

    res.send({
        total: total,
        data: trackingList,
    });
};
