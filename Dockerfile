FROM node:18-alpine as development

WORKDIR /app

COPY package*.json ./
 
RUN npm install -g npm@latest


RUN npm install --legacy-peer-deps 

RUN npm install -g ts-node

COPY . .

RUN npm run build


FROM node:18-alpine as production

WORKDIR /app

ARG NODE_ENV=production

COPY .env .

COPY package*.json ./

RUN npm install --omit=dev --legacy-peer-deps

COPY --from=development /app/dist ./

CMD ["node", "/app/app.js"]
